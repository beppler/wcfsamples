﻿using System.ServiceModel;

namespace WCFSecurity
{
    [ServiceContract(Name = "Echo", Namespace = Constants.ServiceNamespace)]
    public interface IEcho
    {
        [OperationContract]
        string Echo(string message);
    }
}
