﻿using System.Security.Claims;
using System.ServiceModel;

namespace WCFSecurity
{
    [ServiceBehavior(Name = "Echo", Namespace = Constants.ServiceNamespace)]
    public class EchoService : IEcho
    {
        public string Echo(string message)
        {
            return ClaimsPrincipal.Current.Identity.Name + ": " + message;
        }
    }
}